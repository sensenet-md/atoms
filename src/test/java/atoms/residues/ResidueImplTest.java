package atoms.residues;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.residues.Residue;

public class ResidueImplTest {

	private Residue residue;

	@Before
	public void setUp() throws Exception {
		this.residue = createResidue();
	}
		
	private Residue createResidue(){
		Residue residue = Residue.create(1, "MET", "S", "B", "A");
		return residue;
	}

	@Test
	public void testEquals() {
		assertEquals(residue,createResidue());
	}
	
	@Test
	public void testGetName(){
		assertEquals("MET",residue.getName());
	}
	
	@Test
	public void testGetIndex(){
		assertEquals((Integer)1,residue.getIndex());
	}
	
	@Test
	public void testGetResidueInsert(){
		assertEquals("S",residue.getResidueInsert());
	}
	
	@Test
	public void testGetAltLoc(){
		assertEquals("B",residue.getAltLoc());
	}

}
