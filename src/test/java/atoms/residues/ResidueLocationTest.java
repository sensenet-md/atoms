package atoms.residues;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.atoms.AtomLocation;
import com.tcb.atoms.residues.Residue;
import com.tcb.atoms.residues.ResidueLocation;

public class ResidueLocationTest {

	private Residue residue;

	@Before
	public void setUp() throws Exception {
		this.residue = Residue.create(2, "GLU", "C", "", "A");
	}
	
	@Test
	public void testEquals() {
		Residue res = Residue.create(2, "GLU", "C", "", "A");
		
		assertTrue(ResidueLocation.equalLocation(residue, res));
	}
		
	@Test
	public void testEqualsDifferentResidueName() {
		Residue res = Residue.create(2, "GLU", "C", "", "A");
		
		assertTrue(ResidueLocation.equalLocation(residue, res));
				
		res = Residue.create(2, "ASP", "C", "", "A");
		
		assertTrue(ResidueLocation.equalLocation(residue, res));
	}
	
	@Test
	public void testEqualsDifferentResidueInsert() {
		Residue res = Residue.create(2, "GLU", "C", "", "A");
		
		assertTrue(ResidueLocation.equalLocation(residue,res));
				
		res = Residue.create(2, "GLU", "D", "", "A");
		
		assertFalse(ResidueLocation.equalLocation(residue, res));
	}
	
	@Test
	public void testEqualsDifferentResidueAltLoc() {
		Residue res = Residue.create(2, "GLU", "C", "", "A");
		
		assertTrue(ResidueLocation.equalLocation(residue,res));
				
		res = Residue.create(2, "GLU", "C", "A", "A");
		
		assertFalse(ResidueLocation.equalLocation(residue,res));
	}
	
	@Test
	public void testEqualsDifferentResidueChain() {
		Residue res = Residue.create(2, "GLU", "C", "", "A");
		
		assertTrue(ResidueLocation.equalLocation(residue,res));
				
		res = Residue.create(2, "GLU", "C", "", "B");
		
		assertFalse(ResidueLocation.equalLocation(residue, res));
	}

}
