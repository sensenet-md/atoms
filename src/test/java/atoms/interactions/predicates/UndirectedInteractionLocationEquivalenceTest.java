package atoms.interactions.predicates;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.interactions.predicates.UndirectedInteractionLocationEquivalence;

public class UndirectedInteractionLocationEquivalenceTest {

	private String type;
	private Atom source;
	private Atom target;
	private List<Atom> bridge;
	private Timeline timeline;
	private Interaction interaction;
	private UndirectedInteractionLocationEquivalence predicate;

	@Before
	public void setUp() throws Exception {
		this.type = InteractionType.CONTACT.toString();
		this.source = Atom.create("CA", 1, "ARG", "", "", "A");
		this.target = Atom.create("CA", 2, "GLY", "", "", "A");
		this.bridge = Arrays.asList(source);
		this.timeline = Timeline.create(Arrays.asList(1));
		this.interaction = createTestInteraction();
		this.predicate = new UndirectedInteractionLocationEquivalence();
	}
	
	private Interaction createTestInteraction(){
		return Interaction.create(source, target, bridge, timeline, type);
	}

	@Test
	public void testTestSame() {
		assertTrue(predicate.test(createTestInteraction(), interaction));
	}
	
	@Test
	public void testSourceTargetSwitched(){
		Interaction i = Interaction.create(target, source, bridge, timeline, type);
		assertTrue(predicate.test(i, interaction));
	}
	
	@Test
	public void testDifferentType(){
		Interaction i = Interaction.create(source, target, bridge, timeline, "OtherType");
		assertFalse(predicate.test(i, interaction));
	}
	
	@Test
	public void testDifferentBridge(){
		Interaction i = Interaction.create(source, target, Arrays.asList(target), timeline, type);
		assertFalse(predicate.test(i, interaction));
	}
	
	@Test
	public void testDifferentSource(){
		Interaction i = Interaction.create(target, target, bridge, timeline, type);
		assertFalse(predicate.test(i, interaction));
	}
	
	@Test
	public void testDifferentTarget(){
		Interaction i = Interaction.create(source, source, bridge, timeline, type);
		assertFalse(predicate.test(i, interaction));
	}
	
	@Test
	public void testBothAtomsDifferent(){
		Atom a = Atom.create("Dummy", 1, "ARG", "", "", "A");
		Atom b = Atom.create("Dummy2", 1, "ARG", "", "", "A");
		Interaction i = Interaction.create(a, b, bridge, timeline, type);
		assertFalse(predicate.test(i, interaction));
	}

}
