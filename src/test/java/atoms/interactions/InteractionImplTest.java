package atoms.interactions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;

public class InteractionImplTest {

	protected Atom source;
	protected Atom target;
	protected Interaction interaction;

	@Before
	public void setUp() throws Exception {
		this.source = Atom.create("CA", 1, "ARG", "", "", "A");
		this.target = Atom.create("CA", 2, "GLY", "", "", "A");
		this.interaction = createInteraction();
	}
	
	protected Interaction createInteraction(){
		return Interaction.create(
				source,target,Arrays.asList(source),Timeline.create(Arrays.asList(1)),
				InteractionType.CONTACT.toString());
	}
		
	@Test
	public void testEquals() {
		assertEquals(createInteraction(),interaction);
	}
	
	@Test
	public void testGetSourceAtom(){
		assertEquals(source,interaction.getSourceAtom());
	}

	@Test
	public void testGetTargetAtom(){
		assertEquals(target,interaction.getTargetAtom());
	}

	@Test
	public void testGetBridgingAtoms(){
		assertEquals(Arrays.asList(source),interaction.getBridgingAtoms());
	}

	@Test
	public void testGetTimeline(){
		assertEquals(Timeline.create(Arrays.asList(1)), interaction.getTimeline());
	}

	@Test
	public void getType(){
		assertEquals(InteractionType.CONTACT.toString(),interaction.getType());
	}
	

}
