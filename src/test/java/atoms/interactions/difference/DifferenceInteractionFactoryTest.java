package atoms.interactions.difference;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.interactions.difference.DifferenceInteractionFactory;

import atoms.interactions.InteractionImplTest;

import static org.mockito.Mockito.when;

public class DifferenceInteractionFactoryTest extends InteractionImplTest {

	@Override
	protected Interaction createInteraction(){
		Interaction a = Interaction.create(source,target,Arrays.asList(source),Timeline.create(Arrays.asList(1)),
				InteractionType.CONTACT.toString());
		Atom sourceB = Atom.create("CB", 1, "ARG", "", "", "A");
		Atom targetB = Atom.create("CB", 2, "GLY", "", "", "A");
		Interaction b = Interaction.create(sourceB,targetB,Arrays.asList(sourceB),Timeline.create(Arrays.asList(0)),
				InteractionType.CONTACT.toString());
		return DifferenceInteractionFactory.create(a,b);
	}
	
	@Test
	public void testGetTimeline(){
		assertEquals(Timeline.create(Arrays.asList(-1)), interaction.getTimeline());
	}
		

}
