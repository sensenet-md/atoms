package atoms.interactions.difference;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;


import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.interactions.difference.DifferenceInteractionFactory;
import com.tcb.atoms.interactions.difference.DummyInteractionFactory;

import atoms.interactions.InteractionImplTest;

public class DummyInteractionFactoryTest extends InteractionImplTest {

	@Override
	protected Interaction createInteraction(){
		Interaction a = Interaction.create(source,target,Arrays.asList(source),Timeline.create(Arrays.asList(1)),
				InteractionType.CONTACT.toString());
		return DummyInteractionFactory.create(a);
	}
	
	@Test
	public void testGetTimeline(){
		assertEquals(Timeline.create(Arrays.asList(0)), interaction.getTimeline());
	}
		

}
