package atoms.atoms;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.residues.Residue;

public class AtomImplTest {

	private Atom atom;

	@Before
	public void setUp() throws Exception {
		this.atom = createAtom();
	}
	
	private Atom createAtom(){
		Residue residue = createResidue();
		return Atom.create("CA", residue);		
	}
	
	private Residue createResidue(){
		Residue residue = Residue.create(1, "MET", "", "", "A");
		return residue;
	}

	@Test
	public void testEquals() {
		assertEquals(atom,createAtom());
	}
	
	@Test
	public void testGetName(){
		assertEquals("CA",atom.getName());
	}
	
	@Test
	public void testGetResidue(){
		assertEquals(createResidue(),atom.getResidue());
	}

}
