package atoms.atoms;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.atoms.AtomLocation;
import com.tcb.atoms.residues.Residue;
import com.tcb.atoms.residues.ResidueLocation;

public class AtomLocationTest {

	private Atom atom;
	private Residue residue;

	@Before
	public void setUp() throws Exception {
		this.residue = Residue.create(2, "GLU", "C", "", "A");
		this.atom = Atom.create("CA", residue);
	}
	
	@Test
	public void testEquals() {		
		assertTrue(AtomLocation.equalLocation(atom, Atom.create("CA", residue)));
	}
	
	@Test
	public void testEqualsDifferentAtomName() {
		Atom a = Atom.create("CA", residue);
		
		assertTrue(AtomLocation.equalLocation(atom, a));
				
		a = Atom.create("CB", residue);
		
		assertFalse(AtomLocation.equalLocation(atom, a));
	}
	
	@Test
	public void testEqualsDifferentResidueSameLocation() {
		Atom a = Atom.create("CA", residue);
		
		assertTrue(AtomLocation.equalLocation(atom, a));
		
		Residue res = Residue.create(2, "ASP", "C", "", "A");
		a = Atom.create("CA", res);
		
		assertTrue(AtomLocation.equalLocation(atom, a));
	}
	
	@Test
	public void testEqualsDifferentResidueDifferentLocation() {
		Atom a = Atom.create("CA", residue);
		
		assertTrue(AtomLocation.equalLocation(atom, a));
		
		Residue res = Residue.create(3, "GLU", "C", "", "A");
		a = Atom.create("CA", res);
		
		assertFalse(AtomLocation.equalLocation(atom, a));
	}
	
	

}
