package com.tcb.atoms.residues;

public class ResidueLocation {
	public static boolean equalLocation(Residue a, Residue b){
		return a.getIndex().equals(b.getIndex()) &&
				a.getResidueInsert().equals(b.getResidueInsert()) &&
				a.getAltLoc().equals(b.getAltLoc()) &&
				a.getChain().equals(b.getChain());
	}
}
