package com.tcb.atoms.residues;

import java.io.Serializable;
import java.util.Optional;

import com.google.auto.value.AutoValue;
import com.google.auto.value.extension.memoized.Memoized;

import com.tcb.atoms.util.WeakInterner;




@AutoValue
public abstract class Residue implements Serializable {

	private static final long serialVersionUID = 1l;
	
	private final static String keyFormat = "%s/%s%s%d%s";
	private static WeakInterner<Residue> interner = new WeakInterner<>();
	
	public abstract Integer getIndex();
	public abstract String getName();
	public abstract String getResidueInsert();
	public abstract String getAltLoc();
	public abstract String getChain();
	@Memoized
	@Override
	public abstract int hashCode();
	
	public static Residue create(Integer index, String name, String residueInsert, String altLoc, String chain){
		return interner.intern(new AutoValue_Residue(index,name,residueInsert,altLoc,chain));
	}

	@Override
	public String toString(){
		return String.format(keyFormat, getChain(),getName(),getAltLoc(),getIndex(),getResidueInsert());
	}
	
}
