package com.tcb.atoms.atoms;

import java.io.Serializable;
import java.util.Optional;

import com.google.auto.value.AutoValue;
import com.google.auto.value.AutoValue.Builder;
import com.google.auto.value.extension.memoized.Memoized;

import com.tcb.atoms.residues.Residue;
import com.tcb.atoms.util.WeakInterner;



@AutoValue
public abstract class Atom implements Serializable {
	
	private static final long serialVersionUID = 1l;
	
	private static String keyFormat = "%s:%s";
	private static WeakInterner<Atom> interner = new WeakInterner<>();
	
	public abstract String getName();
	public abstract Residue getResidue();
	@Memoized
	@Override
	public abstract int hashCode();
	
	public static Atom create(String name, Residue residue){
		return interner.intern(new AutoValue_Atom(name,residue));
	}
	
	public static Atom create(String name, 
			Integer residueIndex, String residueName, String residueInsert, String altLoc, String chain){
		Residue residue = Residue.create(residueIndex, residueName, residueInsert, altLoc, chain);
		return create(name,residue);
	}

	
	@Override
	public String toString(){
		return String.format(keyFormat, getResidue().toString(), getName().toString());
	}
	

			
}
