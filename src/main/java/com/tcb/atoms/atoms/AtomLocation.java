package com.tcb.atoms.atoms;

import com.tcb.atoms.residues.Residue;
import com.tcb.atoms.residues.ResidueLocation;

public class AtomLocation {
	public static boolean equalLocation(Atom a, Atom b){
		Residue aResidue = a.getResidue();
		Residue bResidue = b.getResidue();
		return ResidueLocation.equalLocation(aResidue, bResidue) &&
				a.getName().equals(b.getName());
	}
}
