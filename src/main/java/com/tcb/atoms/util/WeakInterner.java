package com.tcb.atoms.util;

import com.google.common.collect.Interner;
import com.google.common.collect.Interners;

public class WeakInterner<T> {
	
	private Interner<T> interner;
	
	public WeakInterner(){
		interner = Interners.newWeakInterner();
	}
	
	public T intern(T obj){
		return interner.intern(obj);
	}
}
