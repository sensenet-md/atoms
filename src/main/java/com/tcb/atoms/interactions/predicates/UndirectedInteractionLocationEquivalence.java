package com.tcb.atoms.interactions.predicates;

import java.util.List;
import java.util.function.BiPredicate;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.atoms.AtomLocation;
import com.tcb.atoms.interactions.Interaction;

public class UndirectedInteractionLocationEquivalence implements BiPredicate<Interaction,Interaction> {
	
	@Override
	public boolean test(Interaction a, Interaction b) {
		boolean result = 
				equalTypes(a,b) && 
				equivalentBridgeAtoms(a,b) && 
				equivalentUndirectedSourceTargets(a,b);
		return result;
	}
	
	private boolean equalTypes(Interaction a, Interaction b){
		return a.getType().equals(b.getType());
	}
	
	private boolean equivalentBridgeAtoms(Interaction a, Interaction b){
		List<Atom> aBridge = a.getBridgingAtoms();
		List<Atom> bBridge = b.getBridgingAtoms();
		if(aBridge.size()!=bBridge.size()) return false;
		final int size = aBridge.size();
		for(int i=0;i<size;i++){
			Atom aAtom = aBridge.get(i);
			Atom bAtom = bBridge.get(i);
			if(!AtomLocation.equalLocation(aAtom,bAtom)) return false;
		}
		return true;
	}
	
	private boolean equivalentUndirectedSourceTargets(Interaction a, Interaction b){
		Atom aSource = a.getSourceAtom();
		Atom aTarget = a.getTargetAtom();
		Atom bSource = b.getSourceAtom();
		Atom bTarget = b.getTargetAtom();
		boolean result = 
				(AtomLocation.equalLocation(aSource,bSource) && AtomLocation.equalLocation(aTarget,bTarget))
				||
				(AtomLocation.equalLocation(aSource,bTarget) && AtomLocation.equalLocation(aTarget,bSource));
		return result;
	}

}
