package com.tcb.atoms.interactions.difference;

import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.Timeline;

public class DummyInteractionFactory {
	public static Interaction create(Interaction interaction){
		Timeline timeline = interaction.getTimeline().dummy();
		return Interaction.create( 
				interaction.getSourceAtom(), interaction.getTargetAtom(),
				interaction.getBridgingAtoms(), timeline, interaction.getType());
	}
}
