package com.tcb.atoms.interactions.difference;

import java.util.List;

import com.google.auto.value.AutoValue;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.Timeline;

public class DifferenceInteractionFactory {
	public static Interaction create(Interaction reference, Interaction compared){
		Timeline timeline = compared.getTimeline()
				.getDifference(reference.getTimeline());
		Interaction interaction = Interaction.create(
				reference.getSourceAtom(),
				reference.getTargetAtom(),
				reference.getBridgingAtoms(),
				timeline,
				reference.getType());
		return interaction;
	}
		

}
