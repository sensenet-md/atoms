package com.tcb.atoms.interactions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.auto.value.AutoValue;
import com.google.auto.value.extension.memoized.Memoized;
import com.google.common.collect.ImmutableList;



@AutoValue
public abstract class Timeline implements Serializable {
	
	private static final long serialVersionUID = 2l;
			
	public static Timeline create(List<? extends Number> timeline){
		final int size = timeline.size();
		double[] data = new double[size];
		for(int i=0;i<size;i++){
			data[i] = timeline.get(i).doubleValue();
		}
		return Timeline.create(data);
	}
	
	public static Timeline create(double[] data){
		return new AutoValue_Timeline(data);
	}
	
	public static Timeline create(int[] intData){
		final int size = intData.length;
		double[] data = new double[size];
		for(int i=0;i<size;i++){
			data[i] = (double)intData[i];
		}
		return Timeline.create(data);
	}
	
	public abstract double[] getData();
	@Memoized
	@Override
	public abstract int hashCode();

	public Timeline getDifference(Timeline referenceTimeline) {
		double[] refData = referenceTimeline.getData();
		double[] data = getData();
		if(data.length != refData.length) throw new IllegalArgumentException("Cannot get difference of timelines with differing length.");
		final int size = data.length;
		double[] result = new double[size];
		for(int i=0; i<size; i++){
			result[i] = data[i] - refData[i];
		}
		return Timeline.create(result);
	}
	
	public Timeline dummy(){
		List<Integer> timeline = new ArrayList<>();
		for(int i=0;i<getLength();i++){
			timeline.add(0);
		}
		return Timeline.create(timeline);
	}
	
	
	public Integer getLength() {
		return getData().length;
	}

}
