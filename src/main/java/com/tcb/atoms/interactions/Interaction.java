package com.tcb.atoms.interactions;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.google.auto.value.AutoValue;
import com.google.auto.value.extension.memoized.Memoized;
import com.google.common.collect.ImmutableList;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.residues.Residue;



@AutoValue
public abstract class Interaction implements Serializable {
	
	private static final long serialVersionUID = 1l;

	public abstract Atom getSourceAtom();
	public abstract Atom getTargetAtom();
	public abstract List<Atom> getBridgingAtoms();
	public abstract Timeline getTimeline();
	public abstract String getType();
	@Memoized
	@Override
	public abstract int hashCode();
	
	public static Interaction create(
			Atom source,
			Atom target,
			List<? extends Atom> bridgingAtoms,
			Timeline timeline,
			String type){
		List<Atom> bridgingAtomsLst = ImmutableList.copyOf(bridgingAtoms);
		return new AutoValue_Interaction(source,target,bridgingAtomsLst,timeline,type);
	}

	public String toString(){
		Atom source = getSourceAtom();
		Residue sourceResidue = source.getResidue();
		Atom target = getTargetAtom();
		Residue targetResidue = target.getResidue();
		return String.format("%s#%s/%d:%s_%s_%s/%d:%s", 
				getType(),
				sourceResidue.getChain(),
				sourceResidue.getIndex(),
				source.getName(),
				getBridgingAtoms().stream().map(a -> a.getName()).collect(Collectors.joining(",")),
				targetResidue.getChain(),
				targetResidue.getIndex(),
				target.getName());
	}
}
