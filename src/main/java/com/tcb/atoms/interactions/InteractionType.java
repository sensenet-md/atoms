package com.tcb.atoms.interactions;

public enum InteractionType {
	HBOND, CONTACT, SECSTRUCT;
	
	public String toString(){
		switch(this){
		case HBOND: return "H-bond";
		//$CASES-OMITTED$
		default: return this.name().toLowerCase();
		}
	}
}
